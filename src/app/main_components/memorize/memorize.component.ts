import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Classes } from 'src/app/entities/class.entity';
import { evaluationOptions } from 'src/app/entities/evaluation.entity';
import { typesOfMemorization, typesOfMemorize } from 'src/app/entities/memorizeTypes';
import { Student } from 'src/app/entities/student.entity';
import { BaseService } from 'src/app/services/base.service';
import { amaList, tabarkList } from './dataSet/chapter';

@Component({
  selector: 'app-memorize',
  templateUrl: './memorize.component.html',
  styleUrls: ['./memorize.component.scss'],
})
export class MemorizeComponent implements OnInit {
  constructor(private baseService: BaseService,private fb: FormBuilder, private toastr: ToastrService) {}
  
  typesOfMemorization = typesOfMemorization
  allStudent:Student[] =  [];
  students:Student[] =  []
  async ngOnInit() {
    this.refresh()
    // get chapters from backends
  }

  async refresh(){
    this.createMemorizeForm.setValue({
      card: '',
      chapter: '',
      class: '',
      evaluation: '',
      page: '',
      part: '',
      student: '',
      type: '',
    });
    this.createMemorizeForm.get('student')?.disable();

    this.baseService.get<Classes[]>('class').then((result)=>{
      this.allClass = result  
      this.toastr.success('all classes are fetched')
    }).catch((err)=>{
      this.toastr.error('Can\'t get the classes')
    })

     await this.baseService.get<Student[]>('students')
    .then((result)=>{
      this.allStudent = result
      this.toastr.success('all students are fetched')
    }).catch((err)=>{
      this.toastr.error('Can\'t get the students')
    })
  }

  createMemorizeForm = this.fb.group({
    student: ['', [Validators.required]],
    class: ['', [Validators.required]],
    type: ['', Validators.required],
    part: [''],
    card: [''],
    page: [''],
    chapter: [''],
    evaluation: ['', Validators.required],
  });



  isLoading = false;
  classSelected: any;
  typeSelected: { name: string; code: string } = this.typesOfMemorization[0];
  selectFilterType: typesOfMemorize[] = [];
  selectFilterClass: Classes[] = [];
  selectFilterChapter: any[] = [];
  selectFilterStudent: Student[] = [];

  allClass:Classes[] = [];

  chapters = [
    ...amaList,...tabarkList
  ];

  evaluationOptions = evaluationOptions 
  onChangeClass() {
    this.createMemorizeForm.get('student')?.enable();
    this.students = this.allStudent.filter(
      (student) => student.classs.id == this.classSelected.id
    );
  }

  filterItemsClasses(event: any) {
    this.selectFilterClass = this.allClass.filter((c) =>
      c.name.toLocaleLowerCase().includes(event.query.toLocaleLowerCase())
    );
  }

  filterItemsChapter(event: any) {
    this.selectFilterChapter = this.chapters.filter((c) =>
      c.name.toLocaleLowerCase().includes(event.query.toLocaleLowerCase())
    );
  }

  filterItemsStudent(event: any) {
    this.selectFilterStudent = this.students.filter((c) =>
      c.name.toLocaleLowerCase().includes(event.query.toLocaleLowerCase())
    );
  }

  filterType(event: any) {
    this.selectFilterType = this.typesOfMemorization.filter((c) =>
      c.name.toLocaleLowerCase().includes(event.query.toLocaleLowerCase())
    );
  }

  async saveMemorize() {
    this.isLoading= true    
    if (this.validationMemorizeForm()) {
      let payload = this.getPayload()      
      await this.baseService.post('memorizes/store',payload).then(()=>{
        this.refresh()
        this.toastr.success('Memorize is saved successfully');
      })
    }
    this.isLoading= false
  }

  getPayload(){
    return {
      student_id: this.createMemorizeForm.get('student')?.getRawValue()?.id,
      type: this.typeSelected?.code,
      part: this.createMemorizeForm.get('part')?.value,
      card:this.createMemorizeForm.get('card')?.value,
      page:this.createMemorizeForm.get('page')?.value,
      chapter:this.createMemorizeForm.get('chapter')?.getRawValue()?.name,
      evaluation: this.createMemorizeForm.get('evaluation')?.getRawValue()?.cost
    } 
  }

  validationMemorizeForm() {
    if (
      !this.createMemorizeForm.get('part')?.value &&
      !this.createMemorizeForm.get('card')?.value &&
      !this.createMemorizeForm.get('page')?.value &&
      !this.createMemorizeForm.get('chapter')?.value
    ) {
      this.toastr.error(
        'please add page or chapter or part of card to complete the process.'
      );
      return false;
    }
    if (this.createMemorizeForm.valid) return true;
    const invalidControls = this.findInvalidControls();
    invalidControls.forEach((controller) => {
      switch (controller.name) {
        case 'class':
          this.toastr.error('Please Enter class');
          break;
        case 'student':
          this.toastr.error('Please Enter student');
          break;
        case 'type':
          this.toastr.error('Please choose a type');
          break;
        case 'evaluation':
          this.toastr.error('Please choose the evaluation');
          break;
      }
    });

    return false;
  }

  findInvalidControls() {
    const invalidControlsNames = [];
    let controls: FormControl[] = Object.create(
      this.createMemorizeForm.controls
    );
    for (const name in controls) {
      if (controls[name].invalid) {
        invalidControlsNames.push({
          name: name,
          errors: controls[name].errors,
        });
      }
    }
    return invalidControlsNames;
  }

  changeType() {
    switch (this.typeSelected.code) {
      case 'checkPart':
        this.createMemorizeForm.get('part')?.enable();
        this.createMemorizeForm.get('chapter')?.disable();
        this.createMemorizeForm.get('page')?.disable();
        this.createMemorizeForm.get('card')?.disable();
        break;
      case 'checkCard':
        this.createMemorizeForm.get('part')?.enable();
        this.createMemorizeForm.get('chapter')?.disable();
        this.createMemorizeForm.get('page')?.disable();
        this.createMemorizeForm.get('card')?.enable();
        break;
      case 'advanceMemorize':
        this.createMemorizeForm.get('part')?.enable();
        this.createMemorizeForm.get('chapter')?.disable();
        this.createMemorizeForm.get('page')?.enable();
        this.createMemorizeForm.get('card')?.disable();
        break;
      case 'chapterMemorize':
        this.createMemorizeForm.get('part')?.disable();
        this.createMemorizeForm.get('chapter')?.enable();
        this.createMemorizeForm.get('page')?.disable();
        this.createMemorizeForm.get('card')?.disable();
        break;
      default:
        break;
    }
  }
}
