import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemorizeComponent } from './memorize.component';

describe('MemorizeComponent', () => {
  let component: MemorizeComponent;
  let fixture: ComponentFixture<MemorizeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MemorizeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MemorizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
