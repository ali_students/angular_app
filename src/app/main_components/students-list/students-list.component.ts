import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable, of } from 'rxjs';
import { BaseService } from 'src/app/services/base.service';
import { AddStudentComponent } from '../add-student/add-student.component';
import { UploadStudentsComponent } from '../upload-students/upload-students.component';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss'],
})
export class StudentsListComponent implements OnInit {
  classes: any[] = ['View all', '1', '1', '1', '1', '1', '1', '1', '1'];
  selected_calss = 'View all';

  data: Observable<any> = of([]);

  columns = [
    {
      columnDef: 'id',
      header: 'Id',
      cell: (element: any) => `${element.id}`,
      filter: true,
      width: '40px',
    },
    {
      columnDef: 'name',
      header: 'Name',
      cell: (element: any) => `${element.name}`,
      filter: true,
      width: '40px',
    },
    {
      columnDef: 'phone',
      header: 'Phone',
      filter: true,
      cell: (element: any) => `${element.phone}`,
    },
    {
      columnDef: 'class_name',
      header: 'Class',
      filter: true,
      cell: (element: any) => `${element.class_name}`,
    },
    // {
    //   columnDef: 'time',
    //   header: 'Time',
    //   cell: (element: any) => `${element.time}`,
    // },
  ];

  constructor(private baseService: BaseService, private dialog: MatDialog) {}
  ngOnInit(): void {
    this.get_students();
  }

  async get_students() {
    let students: any = await this.baseService.get('students');
    students = students.map((s: any) => {
      return { ...s, class_name: s.classs.name };
    });
    this.data = of(students);
  }

  add_student() {
    this.dialog
      .open(AddStudentComponent)
      .afterClosed()
      .subscribe((val) => {
      });
  }

  print() {}

  import() {
    this.dialog
      .open(UploadStudentsComponent)
      .afterClosed()
      .subscribe((val) => {
      });
  }
}
