import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BaseService } from 'src/app/services/base.service';
import { DndDirective } from 'src/app/shared/directives/dnd.directive';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-upload-students',
  templateUrl: './upload-students.component.html',
  styleUrls: ['./upload-students.component.scss'],
  standalone: true,
  imports: [CommonModule, DndDirective, FormsModule],
})
export class UploadStudentsComponent implements OnInit {
  classes: any[] = [];
  files: any[] = [];

  loading = true;

  constructor(private baseService: BaseService) {}
  ngOnInit() {
    this.getClass();
  }

  async getClass() {
    const classes: any = await this.baseService.get('class');
    this.classes = classes;
    this.loading = false;
  }

  /**
   * on file drop handler
   */

  onFileDropped($event: any) {
    this.prepareFilesList($event);
  }

  fileBrowseHandler(files: any) {
    this.prepareFilesList(files.files);
  }

  deleteFile(index: number) {
    this.files.splice(index, 1);
  }

  prepareFilesList(files: Array<any>) {
    for (const item of files) {
      item.progress = 0;
      this.files.push(item);
    }
  }

  formatBytes(size: number) {
    const i = Math.floor(Math.log(size) / Math.log(1024));
    let n: number = size / Math.pow(1024, i);
    return parseInt(n.toString()) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
  }

  upload(file: any) {
    this.onFileChange(file);
  }
  onFileChange(file: any) {
    /* wire up file reader */
    let data: any = null;
    const reader: FileReader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (e: any) => {
      /* create workbook */
      const binarystr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(binarystr, { type: 'binary' });

      /* selected the first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      data = XLSX.utils.sheet_to_json(ws); // to get 2d array pass 2nd parameter as object {header: 1}

      if (data) {
        data = data.map((val: any) => {
          return { ...val, classs_id: +file.class_id };
        });
        this.baseService
          .post('students/storeMany', { data: data })
          .then((val) => {
          });
      }
    };
  }
}
