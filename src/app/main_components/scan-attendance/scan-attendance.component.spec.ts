import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScanAttendanceComponent } from './scan-attendance.component';

describe('ScanAttendanceComponent', () => {
  let component: ScanAttendanceComponent;
  let fixture: ComponentFixture<ScanAttendanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScanAttendanceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScanAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
