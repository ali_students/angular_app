import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { BaseService } from 'src/app/services/base.service';

@Component({
  selector: 'app-scan-attendance',
  templateUrl: './scan-attendance.component.html',
  styleUrls: ['./scan-attendance.component.scss'],
})
export class ScanAttendanceComponent implements OnInit {
  callback = this.scan.bind(this);

  list: Observable<any>;

  actions = [
    // { title: 'add', fuction: this.ss.bind(this) },
  ];

  all_att: any[] = [];

  error: string | null;

  cu: any = null;

  columns = [
    {
      columnDef: 'id',
      header: 'Id',
      cell: (element: any) => `${element.id}`,
      filter: true,
      width: '40px',
    },
    {
      columnDef: 'name',
      header: 'Name',
      cell: (element: any) => `${element.name}`,
      filter: true,
      width: '40px',
    },
    {
      columnDef: 'date',
      header: 'Date',
      cell: (element: any) =>
        `${
          element.date.toISOString().substring(0, 10) +
          ' ' +
          element.date.toISOString().substring(11, 16)
        }`,
    },
  ];

  constructor(private base: BaseService) {
    let val = localStorage.getItem('attendance');
    let att: any[] = val ? JSON.parse(val) : [];
    this.all_att = att.length > 0 ? att : [];
  }
  ngOnInit(): void {
  }

  scan(scan: string) {
    const data = JSON.parse(scan);
    alert(scan);

    this.all_att.push({ ...data, date: new Date() });

    this.all_att = [
      ...this.base.removedub2(this.all_att, ['student_id', 'date']),
    ];

    localStorage.setItem('attendance', [...this.all_att].toString());
  }

  upload() {
    this.base
      .post('attendance/storeMany', { data: [...this.all_att] })
      .then((val) => {
        this.all_att = [];
        localStorage.setItem('attendance', [].toString());
      });
  }
}
