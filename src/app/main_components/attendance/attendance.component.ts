import { Component, OnInit } from '@angular/core';
import { BaseService } from 'src/app/services/base.service';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.scss'],
})
export class AttendanceComponent implements OnInit {
  start_date = new Date(2023, 2, 1);
  all_att: any[] = [];

  class: any[] = [];

  columns: any = [
    {
      columnDef: 'id',
      header: 'Id',
      cell: (element: any) => `${element.id}`,
      filter: true,
      width: '40px',
    },
    {
      columnDef: 'name',
      header: 'Name',
      cell: (element: any) => `${element.name}`,
      filter: true,
      width: '40px',
      sticky: true,
    },
  ];

  add() {
    let v = {
      id: 'test',
      name: 'test',
      '3/5': true,
    };
    let r = {
      id: '2',
      name: '12',
      '3/7': true,
    };
    this.all_att.push(v);
    this.all_att.push(r);
    this.all_att = [...this.all_att];
  }

  constructor(private baseService: BaseService) {
    let dates = this.getDaysBetweenDates(this.start_date, new Date());

    dates.map((date) => {
      this.columns.push({
        columnDef: date,
        header: date,
        type: 'check',
        cell: (element: any) =>
          `${
            element[date] == undefined && !element[date] ? false : element[date]
          }`,
        width: '40px',
      });
    });
  }
  ngOnInit(): void {
    this.get_students_attendance();
  }

  async get_students_attendance() {
    let val = await this.baseService.get('students/attendance');
  }

  getDaysBetweenDates(start: Date, end: Date) {
    var result: any[] = [];

    var day = 0;
    var current = new Date(start);

    current.setDate(current.getDate() + ((day - current.getDay() + 7) % 7));

    while (current < end) {
      let dd = new Date(+current);
      result.push(dd.toLocaleDateString().slice(0, -5));
      result.push(
        new Date(new Date(+current).setDate(dd.getDate() + 2))
          .toLocaleDateString()
          .slice(0, -5)
      );
      result.push(
        new Date(new Date(+current).setDate(dd.getDate() + 4))
          .toLocaleDateString()
          .slice(0, -5)
      );
      current.setDate(current.getDate() + 7);
    }
    return result;
  }
}
