import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BaseService } from 'src/app/services/base.service';
import { AddClassComponent } from '../add-class/add-class.component';

@Component({
  selector: 'app-class-list',
  templateUrl: './class-list.component.html',
  styleUrls: ['./class-list.component.scss'],
})
export class ClassListComponent implements OnInit {
  classes: any[] = [];
  loading = true;

  constructor(private baseService: BaseService, private dialog: MatDialog) {}
  ngOnInit() {
    this.getClass();
  }

  async getClass() {
    const classes: any = await this.baseService.get('class');
    this.classes = classes;
    this.loading = false;
  }

  add_class() {
    this.dialog
      .open(AddClassComponent)
      .afterClosed()
      .subscribe((val) => {
        this.classes.push(val);
      });
  }
}
