import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AttendanceComponent } from 'src/app/main_components/attendance/attendance.component';
import { ClassListComponent } from 'src/app/main_components/class-list/class-list.component';
import { MemorizeComponent } from 'src/app/main_components/memorize/memorize.component';
import { ScanAttendanceComponent } from 'src/app/main_components/scan-attendance/scan-attendance.component';
import { StudentsListComponent } from 'src/app/main_components/students-list/students-list.component';

const routes: Routes = [
  {
    path: 'attendance',
    component: AttendanceComponent,
  },
  {
    path: 'scan',
    component: ScanAttendanceComponent,
  },
  {
    path: 'students',
    component: StudentsListComponent,
  },
  {
    path: 'class',
    component: ClassListComponent,
  },
  {
    path: 'memorize',
    component: MemorizeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainLayoutRoutingModule {}
