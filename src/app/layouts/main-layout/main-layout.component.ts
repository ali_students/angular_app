import { Component, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
})
export class MainLayoutComponent {
  open = false;
  lang: string = 'en';
  constructor(private rr: Renderer2) {}
  rev() {
    const val = document.getElementsByTagName('html')[0];
    val.getAttribute('dir') == 'ltr'
      ? this.rr.setAttribute(document.querySelector('html'), 'dir', 'rtl')
      : this.rr.setAttribute(document.querySelector('html'), 'dir', 'ltr');
  }

  change_lang(lang: string) {
    this.rev();
  }
}
