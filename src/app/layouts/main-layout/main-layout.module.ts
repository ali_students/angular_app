import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainLayoutRoutingModule } from './main-layout-routing.module';

import { SidebarComponent } from 'src/app/shared/sidebar/sidebar.component';
import { FooterComponent } from 'src/app/shared/footer/footer.component';
import { ScannerComponent } from 'src/app/shared/scanner/scanner.component';
import { AttendanceComponent } from 'src/app/main_components/attendance/attendance.component';
import { TableComponent } from 'src/app/shared/table/table.component';
import { ScanAttendanceComponent } from 'src/app/main_components/scan-attendance/scan-attendance.component';
import { StudentsListComponent } from 'src/app/main_components/students-list/students-list.component';
import { AddStudentComponent } from 'src/app/main_components/add-student/add-student.component';

import { MatDialogModule } from '@angular/material/dialog';
import { UploadStudentsComponent } from 'src/app/main_components/upload-students/upload-students.component';
import { DndDirective } from 'src/app/shared/directives/dnd.directive';
import { ClassListComponent } from 'src/app/main_components/class-list/class-list.component';
import { AddClassComponent } from 'src/app/main_components/add-class/add-class.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MemorizeComponent } from 'src/app/main_components/memorize/memorize.component';
import {DropdownModule} from 'primeng/dropdown'
import {AutoCompleteModule} from 'primeng/autocomplete'
import {ButtonModule} from 'primeng/button';
import { ToastrModule } from 'ngx-toastr';
const stand_components: any[] = [
  FooterComponent,
  SidebarComponent,
  ScannerComponent,
  TableComponent,
  UploadStudentsComponent,

  DndDirective,
];
const mats = [MatDialogModule];

@NgModule({
  declarations: [
    AttendanceComponent,
    ScanAttendanceComponent,
    StudentsListComponent,
    AddStudentComponent,
    ClassListComponent,
    AddClassComponent,
    MemorizeComponent,
  ],
  imports: [
    ToastrModule.forRoot({ positionClass: 'toast-bottom-right', preventDuplicates: true, newestOnTop: false, progressBar: true }),

    CommonModule,
    MainLayoutRoutingModule,
    CommonModule,
    FormsModule,
    DropdownModule,
    AutoCompleteModule,
    ReactiveFormsModule,
    ButtonModule,
    stand_components,
    mats,
  ],
})
export class MainLayoutModule {}
