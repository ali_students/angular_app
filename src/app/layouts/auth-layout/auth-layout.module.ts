import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthLayoutRoutingModule } from './auth-layout-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { LoginComponent } from 'src/app/auth/login/login.component';
import { MatInputModule } from '@angular/material/input';

import { NgOptimizedImage } from '@angular/common';
import { FormsModule } from '@angular/forms';

const stand_components: any[] = [];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    AuthLayoutRoutingModule,
    stand_components,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    NgOptimizedImage,
    FormsModule,
  ],
})
export class AuthLayoutModule {}
