import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BaseService } from 'src/app/services/base.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  hide: boolean = true;

  login_info: any = {};
  constructor(private baseService: BaseService, private router: Router) {}

  async login() {
    await this.baseService
      .post('login', this.login_info , true)
      .then((val: any) => {
        localStorage.setItem('token', val.token);
      });
      this.router.navigate(['/']);
  }

  logout() {}
}
