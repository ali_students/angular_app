export interface typesOfMemorize {
     name:string
     code:string
}

export const typesOfMemorization:typesOfMemorize[] = [
    {
      name: 'اختبار جزء',
      code: 'checkPart',
    },
    {
      name: 'اختبار بطاقة',
      code: 'checkCard',
    },
    {
      name: 'تسميع متقدم',
      code: 'advanceMemorize',
    },
    {
      name: 'تسميع سورة',
      code: 'chapterMemorize',
    },
  ];