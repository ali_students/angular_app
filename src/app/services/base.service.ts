import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, ReplaySubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BaseService {
  localUrl = 'http://127.0.0.1:8000/api/';
  // base_url = 'https://robin-sass.pioneers.network/api/';
  remoteUrl = 'https://elegant-buck.5-189-175-165.plesk.page/api/';

  loading = new BehaviorSubject(false);

  constructor(private http: HttpClient, private router: Router) {}
get<T>(url: string, loading?: boolean): Promise<T> {
  if (loading) {
    this.loading.next(true);
  }

  const headers = {
    Accept: 'application/json',
    Authorization: 'Bearer ' + localStorage.getItem('token'),
    'Access-Control-Allow-Origin': '*',
  };

  return new Promise<T>((resolve, reject) => {
    this.http.get<T>(this.remoteUrl  + url, { headers }).subscribe(
      (res: T) => {
        if (loading) {
          this.loading.next(false);
        }
        resolve(res);
      },
      (err) => {
        if (loading) {
          this.loading.next(false);
        }
        reject(err);
      }
    );
  });
}
  delete(url: String, loading?: boolean) {
    if (loading) {
      this.loading.next(true);
    }
    return new Promise((resolve, reject) => {
      this.http
        .delete(this.remoteUrl  + url, {
          headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        })
        .subscribe(
          (res) => {
            if (loading) {
              this.loading.next(false);
            }
            return resolve(res);
          },
          (err) => {
            if (loading) {
              this.loading.next(false);
            }
            return reject(err);
          }
        );
    });
  }

  // ------------------------------------------------------------------------------------------

  post(url: String, data: any, loading?: boolean) {
    if (loading) {
      this.loading.next(true);
    }
    return new Promise((resolve, reject) => {
      this.http
        .post(this.remoteUrl  + url, data, {
          headers: {
            Accept: 'application/json',
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        })
        .subscribe(
          (res) => {
            if (loading) {
              this.loading.next(false);
            }
            
            return resolve(res);
          },
          (err) => {
            if (loading) {
              this.loading.next(false);
            }
            return reject(err);
          }
        );
    });
  }

  postFile(data: {
    url: string;
    file: File;
    linked_id: any;
    lniked_type: any;
    document_id: any;
    description: any;
  }) {
    // doc_id 2
    //
    let formData: FormData = new FormData();
    formData.append('file', data.file, data.file.name);
    formData.append('linked_id', data.linked_id);
    formData.append('linked_type', data.lniked_type);
    formData.append('document_id', data.document_id);
    formData.append('description', data.description);
    let header = new HttpHeaders();
    header.append('Authorization', 'Bearer ' + localStorage.getItem('token'));
    return this.http.post(`${this.remoteUrl  + data.url}`, formData, {
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
      responseType: 'text',
    });
  }

  isJson(str: any) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }
  removedub(arr: any[], key: string) {
    return [...new Map(arr.map((item: any) => [item[key], item])).values()];
  }
  removedub2(arr: any[], keys: string[]) {
    return arr.filter(
      (item, index, self) =>
        index ===
        self.findIndex((t) => keys.every((key) => t[key] === item[key]))
    );
  }
}
