import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ZXingScannerModule } from '@zxing/ngx-scanner';
import { BarcodeFormat } from '@zxing/library';

@Component({
  selector: 'app-scanner',
  standalone: true,
  imports: [CommonModule, ZXingScannerModule],
  templateUrl: './scanner.component.html',
  styleUrls: ['./scanner.component.scss'],
})
export class ScannerComponent {
  @Input() callbackFunction: (res: string) => void;

  allowedFormats: BarcodeFormat[] = [
    BarcodeFormat.AZTEC,
    BarcodeFormat.CODABAR,
    BarcodeFormat.CODE_39,
    BarcodeFormat.CODE_93,
    BarcodeFormat.CODE_128,
    BarcodeFormat.DATA_MATRIX,
    BarcodeFormat.EAN_8,
    BarcodeFormat.EAN_13,
    BarcodeFormat.ITF,
    BarcodeFormat.MAXICODE,
    BarcodeFormat.PDF_417,
    BarcodeFormat.QR_CODE,
    BarcodeFormat.RSS_14,
    BarcodeFormat.RSS_EXPANDED,
    BarcodeFormat.UPC_A,
    BarcodeFormat.UPC_E,
    BarcodeFormat.UPC_EAN_EXTENSION,
  ];

  availableDevices: MediaDeviceInfo[];
  currentDevice: MediaDeviceInfo;
  hasDevices: boolean;
  hasPermission: boolean;
  qrResult: any;
  guestExist: boolean;

  constructor() {}

  onCodeResult(resultString: string): void {
    this.playAudio();
    this.guestExist = false;
    this.callbackFunction(resultString);
  }

  playAudio() {
    let audio = new Audio();
    audio.src = '/assets/audio/success.mp3';
    audio.load();
    audio.play();
  }

  onHasPermission(has: boolean): void {
    this.hasPermission = has;
  }

  cc() {
    console.log(' no camra');
  }
}
