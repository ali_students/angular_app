import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { SelectionModel } from '@angular/cdk/collections';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTableExporterModule } from 'mat-table-exporter';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-table',
  standalone: true,
  imports: [
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatTableExporterModule,
    MatCheckboxModule,
    FormsModule,
  ],
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit, AfterViewInit {
  @Input() set data(value: any) {
    if (value != undefined) {
      this.dataSource = new MatTableDataSource(value);
      this.dataSource.filterPredicate = function (record, filter) {
        var map = new Map(JSON.parse(filter));
        let isMatch = true;
        for (let [key, value] of map) {
          let vv = record[key as keyof any] as string;
          vv = isNaN(+vv) ? vv.toLowerCase() : vv.toString();
          isMatch = value == '' || vv.includes(value as string);
          if (!isMatch) return false;
        }
        return isMatch;
      };
    }
  }
  @Input() set col(value: any[]) {
    if (value != undefined) {
      this.columns = value;
      this.displayedColumns = value.map((c) => c.columnDef);
    }
  }
  @Input() set actions(value: any[]) {
    if (value != undefined) {
      if (value.length > 0) {
        this.displayedColumns.push('action');
        this.actions_ops = value;
      }
    }
  }
  @Input() set select(value: boolean) {
    // if (value != undefined) {
    //   if (value.length > 0) {
    //     this.displayedColumns.push('action');
    //     this.actions_ops = value;
    //   }
    // }
  }

  /////////////////////////////       data         ///////////////////////////////
  columns: any[] = [];
  displayedColumns: any[] = [];
  dataSource = new MatTableDataSource<any>([]);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('paginator', { static: true, read: ElementRef })
  pag: ElementRef<HTMLDivElement>;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('table', { static: false, read: ElementRef })
  table: ElementRef<HTMLDivElement>;

  constructor(private renderer: Renderer2) {}
  ngOnInit(): void {
    // ///////////////////     for filter       //////////////////////////
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  /////////////////////////////       execute the function of the action      ///////////////////////////////
  exec(ev: any) {
    ev.fuction();
  }
  /////////////////////////////////////////////////////////////////////////////

  /////////////////////         selection        ////////////////////////////
  clickedRows = new Set<any>();
  selection = new SelectionModel<any>(true, []);
  actions_ops: any[] = [];
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  toggleAllRows() {
    if (this.isAllSelected()) {
      this.selection.clear();
      return;
    }

    this.selection.select(...this.dataSource.data);
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'deselect' : 'select'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${
      row.position + 1
    }`;
  }

  select_row(ev: any, row: any) {
    if (this.select) {
      if ((ev.target?.classList as DOMTokenList).contains('no')) return;
      this.displayedColumns[0] == 'select'
        ? (this.displayedColumns.shift(), this.selection.clear())
        : (this.displayedColumns.unshift('select'), this.selection.toggle(row));
    }
  }

  /////////////////////////////////////////////////////////////////////////////

  // //////////////////////////////   filter ///////////////////////////////////////
  filterDictionary = new Map<string, string>();

  applyEmpFilter(ob: Event, empfilter: any) {
    if ((ob.target as HTMLInputElement).value != '') {
      this.filterDictionary.set(
        (empfilter.columnDef as string).toLocaleLowerCase(),
        (ob.target as HTMLInputElement).value.toLocaleLowerCase()
      );
    } else {
      this.filterDictionary.delete(
        (empfilter.columnDef as string).toLocaleLowerCase()
      );
    }

    var jsonString = JSON.stringify(
      Array.from(this.filterDictionary.entries())
    );
    this.dataSource.filter = jsonString;
  }
}
