import { CommonModule } from '@angular/common';
import {
  Component,
  ElementRef,
  Input,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  standalone: true,
  imports: [FormsModule, CommonModule, RouterModule],
})
export class SidebarComponent {
  sidebarList: any[] = [
    // {
    //   title: 'Dashboard',
    //   route: '',
    //   icon: `            <path
    //           d="M6 8a2 2 0 0 1 2-2h1a2 2 0 0 1 2 2v1a2 2 0 0 1-2 2H8a2 2 0 0 1-2-2V8ZM6 15a2 2 0 0 1 2-2h1a2 2 0 0 1 2 2v1a2 2 0 0 1-2 2H8a2 2 0 0 1-2-2v-1Z"
    //           class="dark:fill-slate-600 fill-current text-sky-600"
    //         ></path>
    //         <path
    //           d="M13 8a2 2 0 0 1 2-2h1a2 2 0 0 1 2 2v1a2 2 0 0 1-2 2h-1a2 2 0 0 1-2-2V8Z"
    //           class="fill-current text-sky-600 group-hover:text-sky-600"
    //         ></path>
    //         <path
    //           d="M13 15a2 2 0 0 1 2-2h1a2 2 0 0 1 2 2v1a2 2 0 0 1-2 2h-1a2 2 0 0 1-2-2v-1Z"
    //           class="fill-current group-hover:text-sky-300"
    //         ></path>`,
    // },
    {
      title: 'Scan',
      route: 'scan',
      icon: `
                  <path
              class="fill-current text-gray-300 group-hover:text-sky-600"
              fill-rule="evenodd"
              d="M4,4h6v6H4V4M20,4v6H14V4h6M14,15h2V13H14V11h2v2h2V11h2v2H18v2h2v3H18v2H16V18H13v2H11V16h3V15m2,0v3h2V15H16M4,20V14h6v6H4M6,6V8H8V6H6M16,6V8h2V6H16M6,16v2H8V16H6M4,11H6v2H4V11m5,0h4v4H11V13H9V11m2-5h2v4H11V6M2,2V6H0V2A2,2,0,0,1,2,0H6V2H2M22,0a2,2,0,0,1,2,2V6H22V2H18V0h4M2,18v4H6v2H2a2,2,0,0,1-2-2V18H2m20,4V18h2v4a2,2,0,0,1-2,2H18V22Z"
              clip-rule="evenodd"
            />
      `,
    },
    {
      title: 'تسميع/اختبار',
      route: 'memorize',
      icon: `
                  <path
              class="fill-current text-gray-300 group-hover:text-sky-600"
              fill-rule="evenodd"
              d="M4,4h6v6H4V4M20,4v6H14V4h6M14,15h2V13H14V11h2v2h2V11h2v2H18v2h2v3H18v2H16V18H13v2H11V16h3V15m2,0v3h2V15H16M4,20V14h6v6H4M6,6V8H8V6H6M16,6V8h2V6H16M6,16v2H8V16H6M4,11H6v2H4V11m5,0h4v4H11V13H9V11m2-5h2v4H11V6M2,2V6H0V2A2,2,0,0,1,2,0H6V2H2M22,0a2,2,0,0,1,2,2V6H22V2H18V0h4M2,18v4H6v2H2a2,2,0,0,1-2-2V18H2m20,4V18h2v4a2,2,0,0,1-2,2H18V22Z"
              clip-rule="evenodd"
            />
      `,
    },
    {
      title: 'Attendance',
      route: '/attendance',
      icon: `
      <path
      class="fill-current text-gray-300 group-hover:text-sky-600"
        d="M6.27,19.43h5.75c.76,0,1.11-1.11,.67-1.89-.33-.59-.87-1.26-1.76-1.64-.51,.35-1.12,.56-1.78,.56s-1.28-.21-1.78-.56c-.88,.38-1.43,1.05-1.76,1.64-.44,.78-.09,1.89,.67,1.89Z"
      />
      <path
      class="fill-current text-gray-300 group-hover:text-sky-600"
        d="M9.14,15.73c1.3,0,2.35-1.05,2.35-2.35v-.56c0-1.3-1.05-2.35-2.35-2.35s-2.35,1.05-2.35,2.35v.56c0,1.3,1.05,2.35,2.35,2.35Z"
      />
      <path
      class="fill-current text-gray-300 group-hover:text-sky-600"
        d="M16.13,2.67h-3.55v.76c0,1.47-1.2,2.67-2.67,2.67h-1.52c-1.47,0-2.67-1.2-2.67-2.67v-.76H2.16C.97,2.67,0,3.63,0,4.83V21.84c0,1.19,.97,2.16,2.16,2.16h13.97c1.19,0,2.16-.97,2.16-2.16V4.83c0-1.19-.97-2.16-2.16-2.16Zm-.13,19.05H2.29V7.05h13.71v14.67h0Z"
      />
      <path
      class="fill-current text-gray-300 group-hover:text-sky-600"
        d="M8.38,5.33h1.52c1.05,0,1.9-.85,1.9-1.9V1.9c0-1.05-.85-1.9-1.9-1.9h-1.52c-1.05,0-1.9,.85-1.9,1.9v1.52c0,1.05,.85,1.9,1.9,1.9Zm-.38-3.43c0-.21,.17-.38,.38-.38h1.52c.21,0,.38,.17,.38,.38v.76c0,.21-.17,.38-.38,.38h-1.52c-.21,0-.38-.17-.38-.38v-.76Z"
      />
      `,
    },
    {
      title: 'Students',
      route: '/students',
      icon: `
      <path
      class="fill-current text-gray-300 group-hover:text-sky-600"
        d="M6.27,19.43h5.75c.76,0,1.11-1.11,.67-1.89-.33-.59-.87-1.26-1.76-1.64-.51,.35-1.12,.56-1.78,.56s-1.28-.21-1.78-.56c-.88,.38-1.43,1.05-1.76,1.64-.44,.78-.09,1.89,.67,1.89Z"
      />
      <path
      class="fill-current text-gray-300 group-hover:text-sky-600"
        d="M9.14,15.73c1.3,0,2.35-1.05,2.35-2.35v-.56c0-1.3-1.05-2.35-2.35-2.35s-2.35,1.05-2.35,2.35v.56c0,1.3,1.05,2.35,2.35,2.35Z"
      />
      <path
      class="fill-current text-gray-300 group-hover:text-sky-600"
        d="M16.13,2.67h-3.55v.76c0,1.47-1.2,2.67-2.67,2.67h-1.52c-1.47,0-2.67-1.2-2.67-2.67v-.76H2.16C.97,2.67,0,3.63,0,4.83V21.84c0,1.19,.97,2.16,2.16,2.16h13.97c1.19,0,2.16-.97,2.16-2.16V4.83c0-1.19-.97-2.16-2.16-2.16Zm-.13,19.05H2.29V7.05h13.71v14.67h0Z"
      />
      <path
      class="fill-current text-gray-300 group-hover:text-sky-600"
        d="M8.38,5.33h1.52c1.05,0,1.9-.85,1.9-1.9V1.9c0-1.05-.85-1.9-1.9-1.9h-1.52c-1.05,0-1.9,.85-1.9,1.9v1.52c0,1.05,.85,1.9,1.9,1.9Zm-.38-3.43c0-.21,.17-.38,.38-.38h1.52c.21,0,.38,.17,.38,.38v.76c0,.21-.17,.38-.38,.38h-1.52c-.21,0-.38-.17-.38-.38v-.76Z"
      />
      `,
    },
    {
      title: 'Class',
      route: '/class',
      icon: `
                        <g id="b">
          <g>
            <path class="fill-current text-gray-300 group-hover:text-sky-600"
              d="M3.82,3.94c1.09,0,1.97-.88,1.97-1.97s-.88-1.97-1.97-1.97S1.85,.88,1.85,1.97c0,1.09,.88,1.97,1.97,1.97Z"
            />
            <path class="fill-current text-gray-300 group-hover:text-sky-600"
              d="M10.51,9.94s0-.08-.01-.12l2.94-2.13c.18-.13,.23-.39,.09-.58-.08-.11-.2-.17-.34-.17-.09,0-.17,.03-.24,.08l-2.95,2.14c-.11-.05-.23-.08-.36-.08h-1.6l-.39-2.21c-.21-1.12-.66-1.99-1.62-1.99H1.99c-.95,0-1.99,1.04-1.99,1.99v6.62c0,.48,.39,.86,.86,.86h.6l.33,5.64c0,.51,.41,.92,.92,.92h1.84c.51,0,.92-.41,.92-.92l.43-11.18h.02l.25,1.09c.08,.52,.53,.9,1.06,.9h2.42c.48,0,.86-.39,.86-.86Zm-7.32-1.23v-3.15h1.13v3.15l-.6,.71-.53-.71Z"
            />
            <polygon class="fill-current text-gray-300 group-hover:text-sky-600"
              points="8.92 .68 8.92 7.87 10.05 7.87 10.05 1.81 22.87 1.81 22.87 12.62 10.05 12.62 10.05 11.26 8.92 11.26 8.92 13.75 24 13.75 24 .68 8.92 .68"
            />
            <path class="fill-current text-gray-300 group-hover:text-sky-600"
              d="M14.62,16.67c0-.88-.71-1.6-1.6-1.6s-1.6,.71-1.6,1.6,.71,1.6,1.6,1.6,1.6-.71,1.6-1.6Z"
            />
            <path class="fill-current text-gray-300 group-hover:text-sky-600"
              d="M13.02,19.12c-1.14,0-2.1,.75-2.47,1.79h4.95c-.37-1.04-1.34-1.79-2.47-1.79Z"
            />
            <path class="fill-current text-gray-300 group-hover:text-sky-600"
              d="M21.5,16.67c0-.88-.71-1.6-1.6-1.6s-1.6,.71-1.6,1.6,.71,1.6,1.6,1.6,1.6-.71,1.6-1.6Z"
            />
            <path class="fill-current text-gray-300 group-hover:text-sky-600"
              d="M19.9,19.12c-1.14,0-2.1,.75-2.47,1.79h4.95c-.37-1.04-1.34-1.79-2.47-1.79Z"
            />
          </g>
        </g>
      `,
    },
  ];

  @Input() set toggle(action: any) {
    if (this.side != undefined) {
      this.side.nativeElement.classList.toggle('ml-[-100%]');
      this.side.nativeElement.classList.toggle('ml-0');
      this.side.nativeElement.classList.toggle('rtl:mr-[-100%]');
      this.side.nativeElement.classList.toggle('rtl:mr-0');
    }
  }
  @ViewChild('side') side: ElementRef | undefined;

  constructor(private rr: Renderer2, private sanitizer: DomSanitizer) {
    this.sidebarList = this.sidebarList.map((val) => {
      val.icon = this.sanitizer.bypassSecurityTrustHtml(val.icon);
      return val;
    });
  }
  change(event: any) {
    event
      ? this.rr.addClass(document.getElementsByTagName('html')[0], 'dark')
      : this.rr.removeClass(document.getElementsByTagName('html')[0], 'dark');
  }
}

// <g><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M0,5.57c.05-.28,.07-.56,.14-.83,.42-1.6,1.34-2.56,2.65-2.97,.2-.06,.42-.08,.63-.12,.06-.01,.13,0,.2,0v1.73c-.37,.06-.73,.18-1.05,.42-.78,.58-1.21,1.41-1.21,2.54,0,4.3,0,8.6,0,12.89,0,1.48,.91,2.69,2.09,2.92,.18,.04,.37,.05,.55,.05,3.67,0,7.34,0,11.01,0,.76,0,1.42-.27,1.96-.93,.45-.55,.68-1.23,.68-2.01,0-.77,0-1.55,0-2.32,0-.07,.01-2.54,.06-2.59,.4-.55,.12-.13,.52-.67,.03-.04,.81-.54,.84-.58,0,0-.03,1.87-.02,1.88,0,.03,.02-1.54,.02-1.51,0,1.55,.02,4.7-.02,6.25-.04,1.84-.82,3.08-2.16,3.84-.45,.25-.93,.37-1.43,.37-3.3,0-6.6,0-9.91,0-.68,0-1.36,.03-2.03-.01-1.46-.1-2.56-.92-3.22-2.54-.16-.39-.25-.81-.29-1.25,0-.04-.01-.09-.02-.13C0,15.18,0,10.38,0,5.57Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M6.03,0c.11,.06,.23,.1,.32,.17,.18,.14,.24,.34,.24,.56,0,.75,0,1.49,0,2.24,0,.06,.03,.14,.07,.18,.35,.4,.32,1.01-.06,1.37-.38,.37-.98,.37-1.37,.01-.39-.36-.43-.97-.08-1.37,.06-.07,.08-.14,.08-.23,0-.71,0-1.41,0-2.12,0-.48,.12-.65,.56-.83h.24Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M9.65,0c.29,.08,.49,.25,.55,.56,.01,.06,.01,.12,.01,.18,0,.73,0,1.47,0,2.2,0,.09,.02,.16,.08,.23,.35,.4,.31,1-.08,1.37-.39,.36-.99,.36-1.37-.01-.38-.37-.41-.97-.06-1.37,.04-.05,.07-.13,.07-.19,0-.71,0-1.41,0-2.12,0-.5,.1-.66,.56-.85h.24Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M13.3,0c.06,.03,.13,.05,.19,.08,.24,.14,.36,.35,.36,.62,0,.75,0,1.5,0,2.25,0,.07,.03,.14,.07,.19,.35,.41,.32,1.01-.07,1.38-.38,.37-.99,.37-1.37,0-.39-.37-.41-.97-.06-1.38,.04-.05,.07-.13,.07-.19,0-.7,.02-1.4,0-2.11C12.47,.43,12.62,.14,13.03,0h.27Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M24,5.56c-.11,.15-.21,.31-.33,.46-.14,.17-.29,.32-.45,.49l-2.25-2.04c.23-.25,.45-.51,.69-.75,.17-.17,.4-.16,.62-.1,.44,.12,.8,.38,1.11,.7,.28,.28,.51,.6,.61,1v.24Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M13.12,11.62c0,1.7-1.38,3.08-3.08,3.08-1.71,0-3.09-1.38-3.09-3.09,0-1.71,1.38-3.08,3.09-3.08,1.71,0,3.08,1.38,3.08,3.08Zm-3.34,.73s-.07-.06-.1-.09c-.37-.33-.73-.65-1.1-.97-.22-.2-.53-.18-.73,.03-.19,.21-.19,.53,.03,.73,.53,.48,1.07,.96,1.61,1.43,.23,.2,.53,.16,.73-.07,.45-.5,.89-1.01,1.34-1.51,.26-.29,.52-.58,.78-.88,.16-.18,.18-.41,.06-.6-.11-.19-.31-.28-.53-.25-.14,.02-.25,.1-.34,.2-.58,.66-1.16,1.31-1.75,1.98Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M20.59,4.89l2.25,2.04-4.34,4.78-2.25-2.04,4.34-4.78Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" class="fill-current text-gray-300 group-hover:text-sky-600" d="M17.66,6.93c0-.46-.01-.91,0-1.36,.05-1.38-1.01-2.37-2.21-2.51-.02,0-.03,0-.05-.01V1.64c.35-.01,.68,.03,1.01,.13,1.35,.41,2.25,1.26,2.58,2.65,.07,.3,.06,.62,.08,.94,0,.03-.01,.07-.04,.1-.44,.49-.89,.97-1.33,1.46,0,0-.01,0-.04,.02Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" class="fill-current text-gray-300 group-hover:text-sky-600" d="M14.84,13.46l1.03-3.37,2.25,2.04-3.28,1.32Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M7.68,14.67h-.08c-1.47,0-2.94,0-4.41,0-.31,0-.5-.26-.4-.53,.06-.15,.19-.24,.35-.24,1.24,0,2.49,0,3.73,0,.04,0,.09,0,.11,.03,.23,.24,.46,.49,.69,.74Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600"  d="M7.47,8.77c-.2,.24-.39,.48-.59,.72-.02,.03-.07,.04-.1,.04-1.21,0-2.42,0-3.63,0-.21,0-.37-.18-.37-.39,0-.21,.16-.37,.37-.37,1.43,0,2.85,0,4.28,0,.02,0,.03,0,.04,0Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M12.38,14.66c.23-.25,.45-.49,.68-.73,.03-.03,.08-.05,.11-.05,.9,0,1.79,0,2.69,0,.22,0,.38,.17,.38,.39,0,.22-.16,.38-.39,.38-.63,0-1.26,0-1.9,0-.49,0-.98,0-1.47,0h-.11Z"/><path class="fill-current text-gray-300 group-hover:text-sky-600" d="M12.6,8.77s.04,0,.06,0c1.06,0,2.12,0,3.18,0,.27,0,.44,.21,.4,.46-.03,.18-.18,.3-.36,.3-.86,0-1.72,0-2.58,0-.03,0-.08-.02-.1-.04-.2-.24-.39-.48-.59-.72Z"/></g><line x1="3.25" y1="19.01" x2="15.82" y2="19.01" stroke-linecap:round; stroke-miterlimit:10; stroke-width:.75px;"/>
