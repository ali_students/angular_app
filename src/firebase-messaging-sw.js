importScripts('https://www.gstatic.com/firebasejs/9.0.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/9.0.0/firebase-messaging-compat.js');




firebase.initializeApp({
  apiKey: "AIzaSyBcswLTy-1iqrRItpplTqD6uRvBfxuMtYI",
  authDomain: "mosque-eeda6.firebaseapp.com",
  projectId: "mosque-eeda6",
  storageBucket: "mosque-eeda6.appspot.com",
  messagingSenderId: "255266441041",
  appId: "1:255266441041:web:81820e0dc82b5e808a3570",
  measurementId: "G-BQKT8KNSS6"
});



const isSupported = firebase.messaging.isSupported();
if (isSupported) {
    const messaging = firebase.messaging();
    messaging.onBackgroundMessage(({ notification: { title, body, image } }) => {
        self.registration.showNotification(title, { body, icon: image || '/assets/icons/icon-72x72.png' });
    });
}
